package com.example.screencapturetest;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.runner.screenshot.BasicScreenCaptureProcessor;
import androidx.test.runner.screenshot.ScreenCapture;
import androidx.test.runner.screenshot.ScreenCaptureProcessor;
import androidx.test.runner.screenshot.Screenshot;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ClickButtonScreenshotTest {

    private static final String TAG = "ClickButtonScreenshotTest";

    private HashSet<ScreenCaptureProcessor> mProcessors = new HashSet<>();
    private MyViewAction mMyViewAction = new MyViewAction();

    @Rule
    public ActivityTestRule<MainActivity> mainActivityRule =
            new ActivityTestRule<>(MainActivity.class);
    @Rule
    public final GrantPermissionRule mGrantPermissionRule =
            GrantPermissionRule.grant(READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE);

    @Before
    public void init() { mProcessors.add(new MyProcessor()); }

    @Test
    public void performClickAndTakeScreenshot() {
        Matcher<View> tvMatcher = withId(R.id.tv_screenshotsNum);

        //set this to however many screenshots you want to take
        int numScreenshots = 5;
        for(int i = 0; i < numScreenshots; i++){
            //increment the text to differentiate between screenshots
            onView(withId(R.id.button)).perform(click());
            //get the text of the TextView
            onView(tvMatcher).perform(mMyViewAction);
            takeScreenshot(mMyViewAction.getText());
        }
    }

    private void takeScreenshot(String fileName) {
        ScreenCapture capture = Screenshot.capture()
                .setFormat(Bitmap.CompressFormat.JPEG)
                .setName(fileName);

        try {
            capture.process(mProcessors);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage() == null ? "no error message" : e.getMessage());
        }
    }
}

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

class MyProcessor extends BasicScreenCaptureProcessor {

    MyProcessor() {
        //deprecated as of api 29. will not work as expected if targeting api 29 or above.
        mDefaultScreenshotPath =
                new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                        "pixolus_screenshots");
    }

    //overriding this method so out file does not get appended with a random UUID.
    //(see "BasicScreenCaptureProcessor" class for more details)
    @Override
    protected String getFilename(String prefix) { return prefix; }
}

////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

class MyViewAction implements ViewAction {

    private final String[] mHolder = { null };

    String getText() { return mHolder[0]; }

    @Override
    public Matcher<View> getConstraints() { return isAssignableFrom(TextView.class); }
    @Override
    public String getDescription() { return "getting text from a TextView"; }
    @Override
    public void perform(UiController uiController, View view) {
        TextView tv = (TextView) view;
        mHolder[0] = tv.getText().toString();
    }
}