package com.example.screencapturetest;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class MainActivity extends AppCompatActivity {

    private int numScreenshots = 0;
    private VideoView mVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView tv_screenshotNum = findViewById(R.id.tv_screenshotsNum);

        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_screenshotNum.setText(String.valueOf(++numScreenshots));
            }
        });

        mVideoView = findViewById(R.id.videoView);
        mVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.vid));

        //todo remove sound

        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mVideoView.start();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkWritePermission();
        mVideoView.start();
    }

    @Override
    protected void onPause() {
        mVideoView.stopPlayback();
        super.onPause();
    }

    private void checkWritePermission() {
        String writePermission = Manifest.permission.WRITE_EXTERNAL_STORAGE;

        if (ContextCompat.checkSelfPermission(this, writePermission)
                != PackageManager.PERMISSION_GRANTED) {
            //Permission is not granted

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, writePermission)) {
                //user needs explanation about the permission
                showPermissionsRationale();
            } else {
                //No explanation needed. request the permission
                ActivityCompat.requestPermissions(this, new String[]{writePermission}, 0);
            }

        } else {
            //Permission is granted.
            //do nothing - left blank for readability.
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        //If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // permission granted.
            //do nothing - left blank for readability.
        } else {
            //permission denied
            showPermissionsRationale();
        }
    }

    private void showPermissionsRationale() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("must allow write permissions for sample app to work")
                .setPositiveButton("go to settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.setData(Uri.fromParts("package", getPackageName(), null));

                        dialog.dismiss();
                        startActivity(intent);
                    }
                });
        builder.show();
    }
}
